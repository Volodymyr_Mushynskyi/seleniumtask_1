package com.epam.utils;

import com.epam.constants.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DriverManager {
    private static WebDriver webdriver;

    protected void initDriver(final String browserName) {

        if ((Constants.CHROME_NAME).equalsIgnoreCase(browserName)) {
            System.setProperty(Constants.CHROME_NAME, Constants.CHROME_DRIVER_LOCATION);
            webdriver = new ChromeDriver();
        }
        webdriver.manage().window().maximize();
        webdriver.manage().timeouts().implicitlyWait(Constants.IMPLICITY_WAIT_VALUE, TimeUnit.SECONDS);
        webdriver.get(Constants.BASE_URL);
    }

    public static WebDriver getDriver() {
        return webdriver;
    }

    protected void quitDriver() {
        if (webdriver != null) {
            webdriver.quit();
            webdriver = null;
        }
    }
}

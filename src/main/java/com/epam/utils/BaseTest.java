package com.epam.utils;

import com.epam.constants.Constants;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class BaseTest extends DriverManager {
    private Logger LOG = Logger.getLogger(String.valueOf(BaseTest.class));
    private Test test;

    @Parameters({"browserName"})
    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(@Optional(value = Constants.CHROME_NAME) final String browserName, Method method) {
        initDriver(browserName);
        BasicConfigurator.configure();
        LOG = LogManager.getLogger(String.valueOf(method.getDeclaringClass()));
        test = method.getAnnotation(Test.class);
        LOG.info(String.format("Test '%s' started.", method.getName()));
        LOG.info(String.format("Description: '%s'", test.description()));
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(final Method method) {
        LOG.info(String.format("Test '%s' completed.", method.getName()));
        quitDriver();
    }
}

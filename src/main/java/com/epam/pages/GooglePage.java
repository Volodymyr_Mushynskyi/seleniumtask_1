package com.epam.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static com.epam.utils.DriverManager.getDriver;

public class GooglePage {

    private WebDriverWait wait = new WebDriverWait(getDriver(), 10);
    private WebElement webElement;

    public void findElementAtThePage(String elementName) {
        webElement = getDriver().findElement(By.name(elementName));
    }

    public void fillCurrentElement(String text) {
        webElement.sendKeys(text);
        webElement.sendKeys(Keys.ENTER);
    }

    public void verifyTitlePage() {
        webElement = wait.until(ExpectedConditions.presenceOfElementLocated((By.tagName("title"))));
        Assert.assertEquals(webElement.getTagName(), "title");
    }

    public void clickByImagesTab() {
        getDriver().findElement(By.xpath("//div[@id='hdtb-msb-vis']//a[@class='q qs' and contains(text(),'Зображення')]")).click();
    }

    public void verifyImagesTabIsOpened() {
        webElement = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='hdtb-mitem hdtb-msel hdtb-imb']")));
        Assert.assertTrue(webElement.isDisplayed());
    }
}

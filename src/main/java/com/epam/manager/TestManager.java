package com.epam.manager;

import com.epam.pages.GooglePage;

public class TestManager {
    public void manageGoogle(){
        GooglePage googlePage = new GooglePage();
        googlePage.findElementAtThePage("q");
        googlePage.fillCurrentElement("Apple");
        googlePage.verifyTitlePage();
        googlePage.clickByImagesTab();
        googlePage.verifyImagesTabIsOpened();
    }
}
